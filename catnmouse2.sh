#!/bin/bash

DEFAULT_MAX_NUMBER=2048

THE_MAX_VALUE=${1:-$DEFAULT_MAX_NUMBER}

echo The max value is $THE_MAX_VALUE

THE_NUMBER_IM_THINKING_OF=$((RANDOM%$THE_MAX_VALUE+1))

#echo The number I\'m thinking of is $THE_NUMBER_IM_THINKING_OF#

echo -n  "OK cat, I'm thinking of a number from 1 to $THE_MAX_VALUE. Make a guess: "

read A_GUESS

while [ $A_GUESS -ne $THE_NUMBER_IM_THINKING_OF ]
do

if [ $A_GUESS -lt 1 ]

then

   echo -n "You must enter a number that's >= 1. Make a guess: "
   read A_GUESS

fi

if [ $A_GUESS -gt $THE_MAX_VALUE ]

then

   echo -n "You must enter a number that's <= $THE_MAX_VALUE. Make a guess: "
   read A_GUESS

fi

if [ $A_GUESS -lt $THE_NUMBER_IM_THINKING_OF ] 

then

   echo -n "No cat... the number I'm thinking of is larger than $A_GUESS. Make a guess: "
   read A_GUESS

fi

if [ $A_GUESS -gt $THE_NUMBER_IM_THINKING_OF ]

then
   echo -n "No cat... the number I'm thinking of is smaller than $A_GUESS. Make a guess: "
   read A_GUESS
   
fi

done

if [ $A_GUESS -eq $THE_NUMBER_IM_THINKING_OF ]

then 
   echo You got me.
   echo ' /\_/\'
   echo "( o.o )"
   echo " > ^ <"
fi

